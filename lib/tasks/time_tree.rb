# Time Tree - Customers, projects and tasks
class TimeTree

  attr_reader :customers, :projects, :tasks, :list

  def initialize
    @list = []
    @customers = []
    @projects = []
    @tasks = []

    # Open a database
    @db = SQLite3::Database.new "lib/tasks/data.db"
    generation_tables
  end

  def generate_list
    @db.execute( "SELECT * FROM jiffy_time_tree" ) do |tree_id, _, parent_id, name, color, archived, _, _, _|
      @list << { tree_id: tree_id, parent_id: parent_id, name: name, color: color, archived: archived } unless name.empty?
    end
  end

  def generate_customer_list
    list.each { |entry| @customers << entry[:tree_id] if entry[:parent_id].nil? }
  end

  def generate_project_list
    list.each { |entry|@projects << entry[:tree_id] if customers.include?(entry[:parent_id])  }
  end

  def generate_tasks_list
    list.each { |entry|@tasks << entry[:tree_id] if projects.include?(entry[:parent_id]) }
  end

  def request
    list.inject([]) { |res, entry| res << check_type(entry) }.join("\n")
  end

  def generation_tables
    generate_list
    generate_customer_list
    generate_project_list
    generate_tasks_list
  end

  private

  def check_type entry
      if customers.include?(entry[:tree_id])
        "Customer.create(id: #{entry[:tree_id]}, name: \"#{entry[:name]}\", color:  \"#{entry[:color]}\")"
      elsif projects.include?(entry[:tree_id])
        "Project.create(id: #{entry[:tree_id]}, name:  \"#{entry[:name]}\", customer_id: #{entry[:parent_id]}, " \
          "color: \"#{entry[:color] ? entry[:color] : 0}\", archived: #{entry[:archived]})"
      else
        "Task.create(id: #{entry[:tree_id]}, name: \"#{entry[:name]}\", project_id: #{entry[:parent_id]}, "\
          "color:  \"#{entry[:color] ? entry[:color] : 0}\", archived: #{entry[:archived]})"
      end
  end

end