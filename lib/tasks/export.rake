require "sqlite3"
require_relative "time_tree.rb"

# Open a database
db = SQLite3::Database.new "lib/tasks/data.db"

namespace :export do
  desc "Prints all contents of databases in a seed.rb way."
  task :seeds_format => :environment do

    time_tree = TimeTree.new

    # Customers, Projects, Tasks
    puts time_tree.request

    # Times - Records of time
    db.execute( "SELECT * FROM jiffy_times" ) do |_, project_id, _, _, _, _, _, _, _, _, start_time, stop_time, _, _, _, _, _, note |
      req = "TimeRecord.create( timeable_id: #{project_id}, start_time: #{start_time}, stop_time: #{stop_time}, note: '#{note ? note.gsub("'"){"\\'"} : nil}',"
      if time_tree.projects.include?(project_id)
        puts req << "timeable_type: \"Project\")"
      elsif time_tree.tasks.include?(project_id)
        puts req << "timeable_type: \"Task\")"
      end
    end

    # Base Work Times
    db.execute( "SELECT * FROM jiffy_base_worktimes" ) do |_, week_day, work_time, _, _ |
      puts "BaseWorktime.create( week_day: #{week_day}, work_time: #{work_time} )"
    end

  end
end

