class SummaryController < ApplicationController

  before_filter :setup

  def index
    if params[:project_id].present?
      @projects = TaskTimeWork.worked_time_per_day @date
    else
      @projects = ProjectTimeWork.worked_time_per_day @date
    end
    @worked  = TimeWork.total_worked @projects
  end

  def week
    if params[:project_id].present?
      @week_stats = TaskTimeWork.week_stats @date
    else
      @week_stats = ProjectTimeWork.week_stats @date
    end
    @projects = TimeWork.worked_time_per_week @week_stats
    @worked = TimeWork.total_worked @projects
  end

  def month
    if params[:project_id].present?
      @month_stats = TaskTimeWork.month_stats @date
    else
      @month_stats = ProjectTimeWork.month_stats @date
    end
    @projects = TimeWork.worked_time_per_week @month_stats
    @worked = TimeWork.total_worked @projects
  end

  def year
    if params[:project_id].present?
      year_stats = TaskTimeWork.year_stats @date
    else
      year_stats = ProjectTimeWork.year_stats @date
    end
    @year_stats = TimeWork.year_stats_grouped year_stats
    @projects = TimeWork.worked_time_per_year @year_stats
    @worked = TimeWork.total_worked @projects
  end

  private

  # set date and project if task view
  def setup
    @date = params[:date] ? Date.parse(params[:date]) : Date.today
    if params[:project_id].present?
      @project = Project.find(params[:project_id])
      TaskTimeWork.set_project @project
    end
  end

end