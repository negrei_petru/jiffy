class AgendaController < ApplicationController
  helper ColorMapperHelper

  def index
    @records = TimeRecord.all.includes(:timeable)
    @records_by_date = @records.group_by(&:date_format)
    @date = params[:date] ? Date.parse(params[:date]) : Date.today

    @worked = TimeWork.worked_time_per_day @date, @records_by_date
    @expected_work = BaseWorktime.expected_work_time @date
  end
end