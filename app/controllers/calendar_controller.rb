class CalendarController < ApplicationController
  def index
    @records = GraphData.calendar_data
  end
end