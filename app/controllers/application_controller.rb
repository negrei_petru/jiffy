class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :week_statistics, :week_graph, :today_statistics

  # needs improvement
  def week_graph
    # hardcoded value
    @project_week_stats ||= ProjectTimeWork.week_stats(Date.today)
    @current_week_stats ||= @project_week_stats.map {|entry| TimeWork.total_worked entry}
    @current_week_stats.map{ |entry| (entry[:sec] / 39600.0) * 100 }
  end

  def today_statistics
    day = Date.today.strftime("%w").to_i + 1
    @current_week_stats[day]
  end

  def week_statistics
    projects = TimeWork.worked_time_per_week @project_week_stats
    TimeWork.total_worked projects
  end

end
