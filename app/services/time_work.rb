class TimeWork

  def self.worked_time_per_week week = week_stats
    worked_time week
  end

  def self.worked_time_per_month month = month_stats
    worked_time month
  end

  def self.worked_time_per_year year = year_stats
    worked_time year
  end

  # harcoded value needs improvement precision
  def self.year_stats_grouped year_stats
    year_stats.each_slice(30).inject([]) { |res, month| res << worked_time(month); res }
  end

  def self.worked_time data
    result = init_data(data)

    data.each do |day|
      day.each {|key,value| result[key][:sec] += value[:sec] }
    end

    result.each { |key, value| result[key] = time_from_seconds(value[:sec]) }
  end

  def self.time_from_seconds seconds
    time = {hour: 0, min: 0, sec:0 }
    time[:sec] = seconds
    unless time[:sec] == 0
      time[:min], _ = time[:sec].divmod(60)
      time[:hour], time[:min] = time[:min].divmod(60)
    end
    time
  end

  def self.total_worked projects
    total_seconds = projects.inject(0) { |sum, (project, time)| sum += time[:sec] }
    time_from_seconds(total_seconds)
  end

  def self.init_data timeables
    # need improvement
    timeables.first.inject({}) { |res, (timable, time)| res[timable] = time_from_seconds(0); res }
  end

  def self.worked_time_per_day day, grouped_by_date
    unless grouped_by_date[day].nil?
      seconds = calculate_worked_seconds(day, grouped_by_date)
      time_from_seconds(seconds)
    else
      time_from_seconds(0)
    end
  end

  def self.calculate_worked_seconds day, grouped_by_date
    grouped_by_date[day].inject(0) do |res, date|
      res += ((date.stop_time.to_time - date.start_time.to_time)).round
    end
  end

end