class GraphData
    include ColorMapperHelper

    def self.data_pie projects
        projects.map do |project, time|
            {project: project.name, time: time[:sec], color: color(project)}
        end
    end

    # needs refactoring
    def self.data_bar data
        counter = 0
        data.map { |entry| calc_data(entry, counter+=1) }
    end

    def self.calc_data entry, counter
        entry.inject({}) do |res, (k,v)|
            res[:state] = counter
            res[k.name] = sec_to_percents(v[:sec])
            res
        end
    end

    def self.sec_to_percents seconds
        # expected time hardcoded at the moment
        (seconds / 1.hour)
    end

    def self.valueFields projects
        projects.inject([]) do |res, (entry, _)|
            res << {valueField: entry.name, color: color(entry), name: entry.name }
            res
        end
    end

    def self.color entry
        GraphData.new.color(entry.color)
    end

    def self.color_type entry
        GraphData.new.color_type entry
    end

    def self.calendar_data
        TimeRecord.all.includes(:timeable).to_a.map do |entry|
            {title: entry.timeable.name, start: entry.start_time, end: entry.stop_time, allDay: false,  color: color_type(entry.timeable)}
        end
    end

end