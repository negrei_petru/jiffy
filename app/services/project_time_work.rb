class ProjectTimeWork

  # Models: Project, TimeRecord
  # sec: represents total number
  # {Project => {hour: min: sec:}}

  def self.worked_time_per_day day = Date.today
    unless grouped_by_date[day].nil?
      result = calculate_worked_seconds(day)
      result.each { |key, value| result[key] = time_from_seconds(value[:sec]) }
    else
      projects
    end
  end

  def self.time_from_seconds seconds
    time = {hour: 0, min: 0, sec:0 }
    time[:sec] = seconds
    unless time[:sec] == 0
      time[:min], _ = time[:sec].divmod(60)
      time[:hour], time[:min] = time[:min].divmod(60)
    end
    time
  end

  def self.week_stats chosen_day
    (chosen_day.at_beginning_of_week..chosen_day.at_end_of_week).map do |day|
      worked_time_per_day day
    end
  end

  def self.month_stats chosen_day
    (chosen_day.at_beginning_of_month..chosen_day.at_end_of_month).map do |day|
      worked_time_per_day day
    end
  end

  def self.year_stats chosen_day
    (chosen_day.at_beginning_of_year..chosen_day.at_end_of_year).map do |day|
      worked_time_per_day day
    end
  end

  def self.calculate_worked_seconds day
    grouped_by_date[day].inject(projects) do |res, record|
      key = check_parent record
      res[key][:sec] += record.time_difference.round
      res
    end
  end

  def self.projects
    @projects ||= Project.all
    @projects.inject({}) { |res, project| res[project] = time_from_seconds(0); res }
  end

  def self.check_parent record
    record.timeable.is_a?(Project) ? record.timeable : record.timeable.project
  end

  def self.grouped_by_date
    @group_by_date ||= TimeRecord.includes(:timeable).group_by(&:date_format)
  end

end