var ready;
ready = function() {

var dataSource = $('#projects_chart').data('projectsBar')

var dataSource_bar = $('#projects_chart').data('projectsPie')

$("#container_bar").dxPieChart({
    dataSource: dataSource_bar,
    title: "",
    tooltip: {
        enabled: true,
        percentPrecision: 2,
        customizeText: function(point) {
            return point.originalArgument + " " + point.percentText;
        }
    },
    legend: { visible: false },
    customizePoint: function (point) {
        return {
            color: dataSource_bar[point.index].color
        }
    },
    series: [{
        type: "doughnut",
        argumentField: "project",
        valueField: "time",
        label: {
            visible: false,
            format: "",
        }
    }]
});

$("#container").dxChart({
    dataSource: dataSource,
    commonSeriesSettings: {
        argumentField: "state",
        type: "stackedBar"
    },
  commonAxisSettings: {
            label: { visible: true },
            grid: { visible: false }
        },
    series: $('#projects_chart').data('valueFields'),
    legend: { visible: false },
    valueAxis: {
        title: {
            text: ""
        }
    },
    argumentAxis: { visible: false },
    title: "",
    tooltip: {
        enabled: true,
        customizeText: function () {
            return this.seriesName;
        }
    }
});
}

$(document).ready(ready);
$(document).on('page:load', ready);