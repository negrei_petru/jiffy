class BaseWorktime < ActiveRecord::Base

  def self.expected_work_time day
    time = { hour: 0, min: 0, sec: 0 }
    miliseconds = find_by_week_day(day.strftime("%w").to_i + 1).work_time

    time[:sec], ms = miliseconds.divmod(1000)
    time[:min], time[:sec] = time[:sec].divmod(60)
    time[:hour], time[:min] = time[:min].divmod(60)
    time
  end

end
