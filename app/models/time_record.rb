class TimeRecord < ActiveRecord::Base
    belongs_to :timeable, polymorphic: true
    def start_time
        DateTime.strptime(read_attribute(:start_time).to_s, '%Q')
    end

    def stop_time
        DateTime.strptime(read_attribute(:stop_time).to_s, '%Q')
    end

    def date_format
        start_time.to_date
    end

    def time_difference
        stop_time.to_time - start_time.to_time
    end
end
