class CreateBaseWorktimes < ActiveRecord::Migration
  def change
    create_table :base_worktimes do |t|
      t.integer :week_day
      t.integer :work_time

      t.timestamps
    end
  end
end
