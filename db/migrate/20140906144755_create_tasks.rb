class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :project, index: true
      t.string :name
      t.string :color
      t.boolean :archived

      t.timestamps
    end
  end
end
