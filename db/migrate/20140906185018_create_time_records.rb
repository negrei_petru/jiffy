class CreateTimeRecords < ActiveRecord::Migration
  def change
    create_table :time_records do |t|
      t.references :timeable, polymorphic: true
      t.integer :start_time
      t.integer :stop_time
      t.text :note

      t.timestamps
    end
  end
end
