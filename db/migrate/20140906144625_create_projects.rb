class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.references :customer, index: true
      t.string :name
      t.string :color
      t.boolean :archived

      t.timestamps
    end
  end
end
