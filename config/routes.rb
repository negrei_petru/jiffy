Rails.application.routes.draw do
    root 'home#index'
    get '/agenda' => 'agenda#index'
    get '/summary' => 'summary#index'
    get '/summary/week' => 'summary#week'
    get '/summary/month' => 'summary#month'
    get '/summary/year' => 'summary#year'
    get '/calendar' => 'calendar#index'
end
